FROM maven:3.5.2-jdk-8-alpine AS MAVEN_TOOL_CHAIN
COPY pom.xml /tmp/
COPY src /tmp/src/
WORKDIR /tmp/
RUN mvn clean tomcat7:run
FROM tomcat:8.5-alpine
COPY --from=MAVEN_TOOL_CHAIN /tmp/repo/com/caucho/quercus/4.0.39/*.war $CATALINA_HOME/webapps/app.war
ADD ./repo/com/caucho/quercus/4.0.39/quercus-4.0.39.jar /usr/local/tomcat/webapps/
ADD pom.xml /usr/local/tomcat/conf/
EXPOSE 8080
CMD chmod +x /usr/local/tomcat/bin/catalina.sh
CMD ["catalina.sh", "run"]
