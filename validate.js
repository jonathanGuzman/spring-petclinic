const xpath = require('xpath')
const dom = require('xmldom').DOMParser
const fs = require('fs')

const XML_FILE_PATH = 'xml/test.xml'
const XPATH_QUERY = '/joblist//job/uuid'

fs.readFile(XML_FILE_PATH, 'utf8', (error, data) => {
    if (error) throw error

    const doc = new dom().parseFromString(data)

    try {
        const nodes = xpath.select(XPATH_QUERY, doc)
        // if we get past this line then the uuid has been found
        console.log("uuid exists: " + nodes[0].firstChild.data)
        console.log("Node: " + nodes[0].toString())
        process.exit(0)
    // on windows when a node is not found it throws an error because of the console.logs calling non-existing attributes
    } catch (err) {
        console.log('uuid not found')
        process.exit(1)
    }
})
